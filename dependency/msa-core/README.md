# Создание Helm chart для приложения msa-core, сборка с использованием команды create и добавлением переменных в файлы values и _helpers

Содержание:
1. <a href="#Stage">Подготовительный этап</a>
2. <a href="#values">Описание файла values.yaml</a>
3. <a href="#help">Описание файла _helpers.tpl</a>
4. <a href="#Persistent">Создание хранилища данных, Persistent Volumes, Persistent Volume Claim</a>
5. <a href="#config">Монтирование конфигураций, Config Map</a>
6. <a href="#Service">Предоставление доступа, Service</a>
7. <a href="#Serviceaccount">Учётная запись для Kubernetes api, Service account</a>
8. <a href="#imsecret">Создание image Pull Secret</a>
9. <a href="#Deployment">Развёртывание, Deployment</a>
10. <a href="#1Helm">Запуск приложения с использованием Helm</a>


<a name="Stage"></a>
## 1. Подготовительный этап 
**Внимание! У вас уже должны быть установлены кластер Kubernetes и клиент Helm.**

Документация по установке кластера Kubernetes: https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/

Официальная документация по установке Helm здесь: https://helm.sh/docs/intro/install/ или здесь https://github.com/helm/helm/releases



**Тестовое приложение с которым будем работать**: https://hub.docker.com/layers/nginxinc/nginx-unprivileged/1.23.3/images/sha256-c4dc191520a3cef56dbf09d6f95c2a950a5b7b932c7476ef372d015c3d5c65fb?context=explore

Переходим в каталог `cd /home/vagrant`, vagrant - имя вашего каталога.
Создаём каталог под названием `helm-test` и переходим в него:
```bash
mkdir helm-test
cd helm-test
```
В текущем каталоге вводим команду для автоматической генерации нужной нам структуры файлов:
`helm create msa-core`

После создания каталога msa-core переходим в него:
```bash
cd msa-core
#Выводим список файлов:
ll
#Видим следующее:
charts/
Chart.yaml
.helmignore
templates/ 
values.yaml #Файл для назначения переменных
```
Chart.yaml cодержит служебную информацию о чарте. Основные поля — version и appVersion. version — версия чарта, следовательно это поле должно меняться при каждой его модификации. appVersion — версия приложения внутри чарта.

Посмотрим содержимое файла Chart.yaml
```bash
sudo vi Chart.yaml

apiVersion: v2
name: msa-core
description: A Helm chart for Kubernetes

# A chart can be either an 'application' or a 'library' chart.
#
# Application charts are a collection of templates that can be packaged into versioned archives
# to be deployed.
#
# Library charts provide useful utilities or functions for the chart developer. They're included as
# a dependency of application charts to inject those utilities and functions into the rendering
# pipeline. Library charts do not define any templates and therefore cannot be deployed.
type: application

# This is the chart version. This version number should be incremented each time you make changes
# to the chart and its templates, including the app version.
# Versions are expected to follow Semantic Versioning (https://semver.org/)
version: 0.1.0

# This is the version number of the application being deployed. This version number should be
# incremented each time you make changes to the application. Versions are not expected to
# follow Semantic Versioning. They should reflect the version the application is using.
# It is recommended to use it with quotes.
appVersion: "1.16.0"
```
<a name="values"></a>
## 2.Описание values.yaml
Файл values.yaml является одним из главных файлов чарта. В нём предоставляется привязка определённого перечня конфигураций к дефолтным значениям.
Рассмотрим нейтральный пример: Есть команда `apt-get install git`.
В файле `values.yaml` привязываем (заменяем) значение git:
`appGit: git`
Теперь наша команда будет выглядеть следующим образом:
```bash
apt-get install {{ .Values.appGit }}
где {{ .Values.appGit }} - указывает что, мы берём значение git из файла values.yaml и этому значению у нас соотвествует навзвание appGit.
#Пример не относится к действительности.
```
Открываем файл values.yaml и добавляем в него переменные, которые будут подставляться в наши yaml файлы. Структура файла уже сгенерирована самим Helm.

```bash
sudo vi values.yaml
## Количество копий
replicaCount: 1
## Образ
image:
  repository: nginxinc/nginx-unprivileged ## Расположение образа
  pullPolicy: IfNotPresent ## Как скачивать
  tag: "1.23.3" ## Версия

## Доступ к репозиторию
imagePullSecrets:
  ## Меняет часть имени
  nameOverride: ""
  ## Меняет имя полностью
  fullnameOverride: ""


## Дока тут https://helm.sh/docs/howto/charts_tips_and_tricks/#creating-image-pull-secrets:~:text=Creating%20Image%20Pull%20Secrets
imageCredentials:
  registry: quay.io ## адрес репо
  username: someone ## Имя пользователя или можно создать токен
  password: sillyness  ## Пароль или можно задать токен
  email: someone@host.com  ## Почта

## Аккаунт для доступа к kubernetes API
serviceAccount:
  create: true
  annotations: {}
  ## Имя для использования Service account
  ## Если имя не укказано, то будет создано автоматически с названием чарта
  name: "" #Задаём название Service account

## Указываем хранилища
persistentVolume:
  enabled: true
  ## Если storage Сlass Name определён, то подставляется значение storageClass
  ## Если неопределён, то будет создан по умолчанию
  ## Если установлен "-", то создаётся динамически
  # storageClass: "helm-store"
  # storageClass: "-"
  accessMode: ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  size: 3Gi
  nodeAffinity: #Настройки https://kubernetes.io/docs/concepts/storage/volumes/#local
    required:
      nodeSelectorTerms:
      - matchExpressions:
          - key: node-role.kubernetes.io/master
            operator: NotIn
            values:
              - ""
  annotations: {}
  ## Имя используемое для обозначения persistent volume
  ## Если значение пустое, то имя создаётся по умолчанию
  name: ""

## Задаём конфигурацию
configMap:
  enabled: true
  defaultConf: ""
#      server {
#        listen       8080 default_server;
#        server_name  _;

#        default_type text/plain;

#        location / {
#            add_header Content-Type text/plain;
#            return 200 'hello perfect World';
#        }
#    }
  annotations: {}
  ## Имя используемое для обозначения configMap
  ## Если значение пустое, то имя создаётся по умолчанию
  name: ""

podAnnotations: {}

podSecurityContext: {}
  # fsGroup: 2000

securityContext:
  # capabilities:
  #   drop:
  #  - ALL
  # readOnlyRootFilesystem: true
  runAsNonRoot: true
  runAsUser: 101

## Доступ к приложениям
service:
  enabled: true
  
  ## Задайте clusterIP статически
  # clusterIP: ""

  ## Задайте nodePort
  nodePort: ""

  ## Задайте loadBalancer, здесь указывается необходимый ip адрес
  # loadBalancer: ""
  ## Указывается список диапазонов IP адресов для доступа к службе
  # loadBalancerSourceRanges: []

  ## Указываем один из типов ClusterIP, NodePort, Loadbalancer
  type: NodePort


  metrics:
    ## Если используется NodePort то необходимо указать nodePort
    nodePort: 32554

    ## При необходимости можно указать список дополнительных портов
    # additionalPorts: [] 

metrics:
  enabled: true

  ## Определить имя для порта
  ##
  portName: metrics

  ## Задать номер внутреннего порта
  ##
  port: 8080

## Для подтягивания доменного имени
ingress:
  enabled: false
  className: ""
  annotations: {}
    # kubernetes.io/ingress.class: nginx
    # kubernetes.io/tls-acme: "true"
  hosts:
    - host: chart-example.local
      paths:
        - path: /
          pathType: ImplementationSpecific
  tls: []
  #  - secretName: chart-example-tls
  #    hosts:
  #      - chart-example.local

## Задаём ОЗУ и ЦП
resources:
  limits:
    cpu: 300m
    memory: 512Mi
  requests:
    cpu: 100m
    memory: 256Mi

## Добавление ресурсов при необходимости
autoscaling: {}
  # enabled: false
  # minReplicas: 1
  # maxReplicas: 100
  # targetCPUUtilizationPercentage: 80
  # targetMemoryUtilizationPercentage: 80

## Указываем на какой НОДе запустить ПОДы
nodeSelector: {}
## Допуски применимые к ПОДам
tolerations: []

## Привязка к узлу
affinity: {}
```

<a name="help"></a>
## 3. Описание файла _helpers.tpl
Это файл с шаблонами, которые переиспользуются в чарте. Данная связь позволяет использовать один и тот же чарт несколько раз в одном пространстве имён.
Переходим в каталог `/home/vagrant/helm-test/msa-core/templates` и создаём в нём недостающие файлы в формате `yaml` для развертывания приложения:
```bash
cd /home/vagrant/helm-test/msa-core/templates
touch persistentvolumeclaim.yaml
touch persistentvolume.yaml
touch configmap.yaml
touch imagepullsecurity.yaml
```
Выведем список файлов в каталоге `templates`:
```bash
ll
#Вывод:
configmap.yaml
deployment.yaml
_helpers.tpl
hpa.yaml
ingress.yaml
NOTES.txt
persistentvolumeclaim.yaml
persistentvolume.yaml
serviceaccount.yaml
service.yaml
imagepullsecurity.yaml
tests/
```
Содержимое файла _helpers.tpl
```bash
sudo vi _helpers.tpl

{{/*
Expand the name of the chart. ## Имя Чарта
*/}}
{{- define "msa-core.name" -}} ## Определить как имя msa-core
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }} ## максимум 63 символа, trimSuffix "-"  удаляет завершающий символ
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "msa-core.fullname" -}} ## Задать msa-core как полное имя
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "msa-core.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }} ## Заменить + на -
{{- end }}

{{/*
Common labels
*/}}
{{- define "msa-core.labels" -}}
helm.sh/chart: {{ include "msa-core.chart" . }}
{{ include "msa-core.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "msa-core.selectorLabels" -}}
app.kubernetes.io/name: {{ include "msa-core.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "msa-core.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "msa-core.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

#{{/*
#Create the name of the persistent volume to use
#*/}}
#{{- define "msa-core.persistentVolume" -}}
#{{- if .Values.persistentVolume.create }}
#{{- default (include "msa-core.fullname" .) .Values.persistentVolume.name }}
#{{- else }}
#{{- default "default" .Values.persistentVolume.name }}
#{{- end }}
#{{- end }}

#{{/*
#Create the name of the persistent volume claim to use
#*/}}
#{{- define "msa-core.persistentVolumeClaim" -}}
#{{- if .Values.persistentVolumeClaim.create }}
#{{- default (include "msa-core.fullname" .) .Values.persistentVolumeClaim.name }}
#{{- else }}
#{{- default "default" .Values.persistentVolumeClaim.name }}
#{{- end }}
#{{- end }}

#{{/*
#Create the name of the config map to use
#*/}}
#{{- define "msa-core.configMap" -}}
#{{- if .Values.configMap.create }}
#{{- default (include "msa-core.fullname" .) .Values.configMap.name }}
#{{- else }}
#{{- default "default" .Values.configMap.name }}
#{{- end }}
#{{- end }}

{{/*
Create the name of the image Pull Secret to use
*/}}
{{- define "imagePullSecret" }}
{{- with .Values.imageCredentials }}
{{- printf "{\"auths\":{\"%s\":{\"username\":\"%s\",\"password\":\"%s\",\"email\":\"%s\",\"auth\":\"%s\"}}}" .registry .username .password .email (printf "%s:%s" .username .password | b64enc) | b64enc }}
{{- end }}
{{- end }}
```

Рассмотрим отрывок из файла _helpers.tpl и файл serviceaccount.yaml:
![](./images/help-service.jpg)


<a name="Persistent"></a>
## 4. Создание хранилища данных, Persistent Volumes, Persistent Volume Claim
### 4.1 Persistent Volumes (PV)
Создадим **Persistent Volumes (PV)** - это ресурс, часть хранилища в кластере. Не имеет прострастсва имён. Можно описать как отдельно создаваемую НОДу или некий аналог НОДы. Монтируемое хранилище данных.
Данное хранилище данных подготавливается с помощью классов хранилища (Storage Classes) https://kubernetes.io/docs/concepts/storage/storage-classes/

Существует два способа создания Persistent Volumes:
- Статический - администратор кластера создаёт persistent volumes файлы, которые содержат информацию о хранилище. Хранилище доступно и используется пользователями;
- Динамический - если созданное из статических persistent volumes хранилище не соответсвует запросам (Persistent Volume Claim) пользователя, то они будут созданы автоматически.

**Persistent Volumes** подключаются с помощью плагинов.
В настоящее время Kubernetes поддерживает множество плагинов. Посмотреть их можно здесь: https://kubernetes.io/docs/concepts/storage/persistent-volumes/#types-of-persistent-volumes

В шаблоны добавлена метка с условиями. Разберём её на примере Persistent Volume файлов persistentvolume.yaml и values.yaml:
Шаблон указан в целях ознакомления, частично не соотвествует параметрам представленным ниже.
![](images/ps-values.jpg)

Зададим параметры из файла values.yaml для persistentvolume
```bash
sudo vi persistentvolume.yaml

{{- if .Values.persistentVolume.enabled -}} ###Если PersistentVolume активен, значение enabled: true в values.yaml.
apiVersion: v1
kind: PersistentVolume
metadata:
  name: {{ include "msa-core.fullname" . }}-pv ## Название чарта и окончание -pv, будет выглядеть msa-core-pv
  labels:
    {{- include "msa-core.labels" . | nindent 4 }}  ## Добавление меток
  {{- with .Values.labels }} 
    {{- toYaml . | indent 4 }}
  {{- end }}
  {{- with .Values.podAnnotations }}
  annotations:
    {{- toYaml . | nindent 4 }}
{{- end }}
spec:
  capacity:
    storage: {{ .Values.persistentVolume.size }} ## Параметр размер хранилища задан в values.yaml
  accessModes:
    - {{ .Values.persistentVolume.accessMode | quote }}  ## Режим доступа указан в values.yaml
  persistentVolumeReclaimPolicy: {{ .Values.persistentVolume.persistentVolumeReclaimPolicy }}  
  local: ## Плагин local можно изменить на hostPath, тогда удаляем nodeAffinity и все условия к нему
    path: /mnt ## Данный каталог монтируется на нашем пк, с которого всё запускается
  {{- with .Values.persistentVolume.nodeAffinity }}
  nodeAffinity: #Настройки https://kubernetes.io/docs/concepts/storage/volumes/#local
    {{- toYaml . | nindent 8 }}
  {{- end }}
{{- if not .Values.persistentVolume.storageClass }}  ## Если нет StorageClass, задать fullname чарта
  storageClassName: "{{ include "msa-core.fullname" . }}"
{{- end }}
{{- if .Values.persistentVolume.storageClass }} 
{{- if (eq "-" .Values.persistentVolume.storageClass) }}  ## Если указан "-" StorageClass, задать fullname-dyn чарта
  storageClassName: "{{ include "msa-core.fullname" . }}-dyn"
{{- else }}
  storageClassName: {{ .Values.persistentVolume.storageClass }} ## Если указан "helm-storage" StorageClass, задать "helm-storage"
{{- end }}
{{- end }}
{{- end }}
```
Рекомендованный режим доступа указанный в официальной документации для плагина hostPath, также мы можем его использовать и для плагина local. https://kubernetes.io/docs/concepts/storage/persistent-volumes/#types-of-persistent-volumes:~:text=%2D-,HostPath,-%E2%9C%93

`persistentVolumeReclaimPolicy` - указывает что будет с pv после удаления pvc. Возможны 3 варианта:
- Retain - PersistentVolume удален не будет;
- Recycle - PersistentVolume будет очищен;
- Delete - PersistentVolume будет удален.

Необходимо запомнить следующие параметры:
- StorageClassName - он у нас задан в файле values.yaml, указано имя msa-storage. Данное имя будет использовано в файлах где указаны типы PersistentVolume, StorageClass, PersistentVolumeClaim;
- В Capacity указан storage размером 3Gi. При создании PersistentVolumeClaim, данный параметр не может быть выше чем он задаётся в PersistentVolume, то есть нельзя задать выше 3Gi;
- AccessModes должны совпадать в PersistentVolume и PersistentVolumeClaim, у нас задан доступ ReadWriteOnce.
### 4.2 Persistent Volume Claim (PVC)
Также создадим **Persistent Volume Claim (PVC)** — это запрос на хранение данных пользователем, работает в связке с Persistent Volumes для создания статического хранилища. Он похож на Pod. Модули могут запрашивать определенные уровни ресурсов (ЦП и память). Также запрашиваются определенный размер и режимы доступа (например, они могут быть смонтированы ReadWriteOnce, ReadOnlyMany или ReadWriteMany). Режимы доступа Access Modes смотрим здесь: https://kubernetes.io/docs/concepts/storage/persistent-volumes/#access-modes


В данном шаблоне всё происходит по аналогии с Persistent Volumes.
Значения подставляются такие же как для Persistent Volumes.
```bash
sudo vi persistentvolumeclaim.yaml

{{- if .Values.persistentVolume.enabled -}}
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: {{ include "msa-core.fullname" . }}-pvc
  labels:
    {{- include "msa-core.labels" . | nindent 4 }}
  {{- with .Values.labels }}
    {{- toYaml . | indent 4 }}
  {{- end }}
  {{- with .Values.podAnnotations }}
  annotations:
    {{- toYaml . | nindent 4 }}
{{- end }}
spec:
  accessModes:
    - {{ .Values.persistentVolume.accessMode | quote }}
  resources:
    requests:
      storage: {{ .Values.persistentVolume.size | quote }}
{{- if not .Values.persistentVolume.storageClass }}  
  storageClassName: "{{ include "msa-core.fullname" . }}"
{{- end }}
{{- if .Values.persistentVolume.storageClass }}
{{- if (eq "-" .Values.persistentVolume.storageClass) }}
  storageClassName: "{{ include "msa-core.fullname" . }}-dyn"
{{- else }}
  storageClassName: {{ .Values.persistentVolume.storageClass }} 
{{- end }}
{{- end }}
{{- end }}
```
<a name="config"></a>
## 5. Монтирование конфигураций, Config Map
**Config Map** - данный тип монтирования был придуман для отвязки конфигураций контейнера от deployment для более удобной транспортировки приложений. Данная абстракция позволяет уменьшить размер файла deployment. Используется для хранения данных не требующих секретности.
Для обеспечения секретности вместо Config Map рекомендуется использовать Secret.
Подробнее можно почитать здесь: https://kubernetes.io/docs/concepts/configuration/configmap/

Шаблон описан как и предыдущие.
Откроем созданный нами ранее файл и внесём изменения:
```bash
sudo vi configmap.yaml

{{- if .Values.configMap.enabled -}} ## Если, создан Config Map, то значения брать из файла values.yaml
apiVersion: v1 
kind: ConfigMap
metadata:
  name: {{ include "msa-core.fullname" . }}-cm ## Название чарта и окончание -cm, будет выглядеть msa-core-cm
  labels:
    {{- include "msa-core.labels" . | nindent 4 }} ## Назначение меток
  {{- with .Values.configMap.annotations }}
  annotations:
    {{- toYaml . | nindent 4 }}
  {{- end }}
{{- end }}
data:
  default.conf: |-
{{- if .Values.configMap.defaultConf }}
{{ tpl .Values.configMap.defaultConf . | indent 4 }}
{{- else }}
{{ .Files.Get "files/default.conf" | indent 4 }}
{{- end }}



## {{- if .Values.configMap.defaultConf }} - если имеется значение Values.configMap.defaultConf, подставить данные из values.yaml
## {{- else }} {{ .Files.Get "files/default.conf" | indent 4 }} - также значения подставить можно по пути files/default.conf
## папка с файлом находится в нашем каталоге с проектом
## Примечание! При использовании данных парамертов, одно из условий необхиодимо отключить. 
## Например задав другой путь к каталогу или закомментировать значения в файле values.yaml
## default.cong - файл находящийся в контейре по пути /etc/nginx/conf.d/default.conf, 
## значения которые будут заменены представлены в файле values.yaml или в папке files:

```
<a name="Service"></a>
## 6. Предоставление доступа, Service
**Service** - определяет набор модулей и политик предоставляющих доступ к ПОДу, сети, группе ПОДов. Service распознаёт ПОД по заданным меткам. 
Сущесвуте 4 категории Service:
- ClusterIP - стандартная служба, все выполняемые действия доступны только внутри сети или кластера;
- NodePort - каждой НОДе в кластере предоставляет порт для доступа;
- LoadBalancer - один из самых популярных способов, который позволяет предоставить доступ через сеть Интернет;
- ExternalName - перенапраляет трафик во внешнюю службу.

Официальная документация для подробного ознакомелния: https://kubernetes.io/docs/concepts/services-networking/service/

Откроем файл service.yaml для просмотра , он сгенерирован автоматически самим Helm:
```bash
sudo vi service.yaml

{{- if and .Values.service.enabled .Values.metrics.enabled -}} ## Если сервис и метрики включены подставить значения
apiVersion: v1
kind: Service
metadata:
  name: {{ include "msa-core.fullname" . }}
  labels:
    {{- include "msa-core.labels" . | nindent 4 }}
  {{- with .Values.labels }} 
    {{- toYaml . | indent 4 }}
  {{- end }}
  {{- with .Values.podAnnotations }}
  annotations:
    {{- toYaml . | nindent 4 }}
{{- end }}
spec:
  {{- if .Values.service.clusterIP }}  ## Если стоит значение clusterIP, то использовать параметры для него
  clusterIP: {{ .Values.service.clusterIP | quote }}
  {{- end }}
  {{- if .Values.service.nodePort }}  ## Если стоит значение nodePort, то использовать значения для него
  nodePort :
    {{- toYaml .Values.service.nodePort | nindent 4 }}
  {{- end }}
  {{- if .Values.service.loadBalancer }}  ## Если стоит значение loadBalancer, то использовать значения для него
  loadBalancerI: {{ .Values.service.loadBalancerIP | quote }}
  {{- end }}
  {{- if .Values.service.loadBalancerSourceRanges }} ## Также можно задать диапазон IP адресов
  loadBalancerSourceRanges:
  {{- range $cidr := .Values.service.loadBalancerSourceRanges }}
    - {{ $cidr | quote }}
  {{- end }}
  {{- end }}
  ports:
  {{- if .Values.metrics.enabled }}  ## Если метрики включены то подставить следующее для NodePort
  - name: {{ .Values.metrics.portName | quote }}  ## Имя порта
    {{- if eq .Values.service.type "NodePort" }}  ## Если значение соответсвует типу NodePort
    nodePort: {{ .Values.service.metrics.nodePort }}  ## Задать внешний порт из values.yaml
    {{- end }}
    port: {{ .Values.metrics.port }}   ## Задать внутренний порт
    targetPort: {{ .Values.metrics.portName | quote }}
  {{- end }}
  {{- if .Values.service.additionalPorts }}  ## Задать дополнительные порты
  {{- toYaml .Values.service.additionalPorts | nindent 2 }}
  {{- end }}
  selector:
    {{- include "msa-core.selectorLabels" . | nindent 4 }}
  type: {{ .Values.service.type | default "ClusterIP" | quote }}
  {{- end }}
```

<a name="Serviceaccount"></a>
## 7. Учётная запись для Kubernetes api, Service account
Официальная документация: https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/
**Service Account** - это учётная запись, позволяющая общаться приложениям с kubernetes api (Application Programming Interface) без предоставления данных о текущем пользователе.
Основное назначение предоставить процессам внутри пода доступ к api.
Откроем файл serviceaccount.yaml для просмотра , он сгенерирован автоматически самим Helm:
```bash
sudo vi serviceaccount.yaml

{{- if .Values.serviceAccount.create -}} #Если будет создан ServiceAccount, то применяются значения из values.yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: {{ include "msa-core.serviceAccountName" . }}-sa #Определяет имя представленное в файле values.yaml
  labels:
    {{- include "msa-core.labels" . | nindent 4 }} #Общие переменные для меток
  {{- with .Values.serviceAccount.annotations }} #При отсутствии имени, генерирует его 
  annotations:
    {{- toYaml . | nindent 4 }} #Копирует всё из поддерева
  {{- end }}
{{- end }} #Завершить выполнение
```
<a name="imsecret"></a>
## 8. Создание image Pull Secret

https://kubernetes.io/docs/concepts/configuration/secret/#using-imagepullsecrets

Helm chart Документация: https://helm.sh/docs/howto/charts_tips_and_tricks/#creating-image-pull-secrets:~:text=Creating%20Image%20Pull%20Secrets

**Секреты** используются для хранения скрытой информации, пароли, токены, ключи и т.д.

```bash
sudo vi imagepullsecrets.yaml

apiVersion: v1
kind: Secret
metadata:
  name: {{ include "msa-core.fullname" . }}-sec
type: kubernetes.io/dockerconfigjson
data:
  .dockerconfigjson: {{ template "imagePullSecret" . }} ## Значения берутся из _helpers.tpl
```
<a name="Deployment"></a>
## 9. Развёртывание, Deployment
**Deployment** – это ресурс Kubernetes, предназначенный для развертывания приложений и их обновления декларативным образом, вместо того чтобы делать это через ReplicationController или ReplicaSet, которые рассматриваются как более низкоуровневые понятия.
Официальная документация: https://kubernetes.io/docs/concepts/workloads/controllers/deployment/

Откроем уже существующий и сгенерированный Helm файл deployment.yaml, здесь были добавлены монтируемые тома:
```bash
sudo vi deployment.yaml

apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "msa-core.fullname" . }}
  labels:
    {{- include "msa-core.labels" . | nindent 4 }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "msa-core.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "msa-core.selectorLabels" . | nindent 8 }}
    spec:
      imagePullSecrets:
        - name: {{ include "msa-core.fullname" . }}-sec ## Задаём название секрета
      volumes:
        - name: {{ include "msa-core.fullname" . }}
          persistentVolumeClaim:
            claimName: {{ include "msa-core.fullname" . }}-pvc ## Ссылаемся на Persistent Volume Claim
        - name: {{ include "msa-core.fullname" . }}-cm
          configMap:
            name: {{ include "msa-core.fullname" . }}-cm ## Ссылаемся на configMap
      serviceAccountName: {{ include "msa-core.serviceAccountName" . }}-sa ## сылаемся на serviceAccount
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }} 
          ports:
            - name: {{ .Values.metrics.portName | quote }} ## Указываем имя порта для сервиса из values.yaml
              containerPort: {{ .Values.metrics.port }} ## Указываем порт
              protocol: TCP
          volumeMounts:
            - name: {{ include "msa-core.fullname" . }} ## Задаём имена монтируемых томов
              mountPath: /mnt
            - name: {{ include "msa-core.fullname" . }}-cm ## Задаём имена монтируемых томов
              mountPath: /etc/nginx/conf.d/default.conf ## Путь к файлу CM
              subPath: default.conf
          resources:
            {{- toYaml .Values.resources | nindent 12 }}  ## Подставляем значения ОЗУ и ЦП
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
```
<a name="1Helm"></a>
## 10. Запуск приложения с использованием Helm
После того как были выполнены все выше указанные действия, мы можем попробовать запустить наш Helm chart.
Переходим в каталог `/home/vagrant/helm-test/msa-core` и находясь в нём вводим следующие команды:
`cd /home/vagrant/helm-test/msa-core/`

Создадим helm chart введя команду:
```bash
helm install msa-core . 
#msa-core - задаём название нашему Helm chart;
#. - точка означает запустить из текущего каталога.
```
Также можно создать Helm chart задав ему пространство имён (namespace), показано для примера, мы не будем использовать.
Вводим команду:
```bash
helm install msa-core . --namespace msa-core --create-namespace
#msa-core - задаём название нашему Helm chart;
#. - точка означает запустить из текущего каталога;
#msa-core - задаём название namespace.
```
Посмотрим запустился ли наш helm chart:
```bash
helm list
#Вывод:
NAME            NAMESPACE       REVISION        UPDATED                                 STATUS          CHART           APP VERSION
msa-core        msa-core         1               2022-12-27 13:57:54.212795971 +0000 UTC deployed        msa-core-0.1.0  1.16.0

#Видим Status - deployed
```
Проверим поднялись и привязались ли наши PV и PVC:
```bash
kubectl get pv
#Вывод:
NAME         CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                      STORAGECLASS       REASON   AGE
sa-core-pv   3Gi        RWO            Retain           Bound    msa-core/msa-core-pvc          msa-core            21m


kubectl get pvc
#Вывод:
NAME               STATUS   VOLUME       CAPACITY   ACCESS MODES   STORAGECLASS       AGE
msa-core-pvc         Bound    msa-core-pv   3Gi        RWO            msa-core   21m


#Видим, что статус у обоих в состоянии bound, что значит связаны.
#Названия storage class совпадают. 
#Видим что pv и pvc ссылыаются на наименования volume и claim. 
```
Проверим Service account:
```bash
kubectl get sa
NAME       SECRETS   AGE
msa-core-sa    0         12m
```
Посмотрим на наш Config Map:
```bash
kubectl get cm
#Вывод:
NAME               DATA   AGE
msa-core-cm      1      21m
```
Выведем список запущенных ПОД:
```bash
kubectl get pods -n msa-core
#Вывод:
NAME                                READY   STATUS      RESTARTS   AGE
msa-core-f7d95d75b-xhpfx   1/1     Running     0          22m

#msa-core-f7d95d75b-xhpfx - наименование вашего ПОДа;
#READY 1/1 - ПОД запущен;
#STATUS RUNNING - всё работает.
```
Проверим соотвествует ли ПОД заданным параметрам:
```bash
kubectl describe pod/msa-core-f7d95d75b-xhpfx
#nginx-msa-deploy-6d84b867b5-jbt7x наименование вашего ПОДа.
#Вывод, смотрим у себя выделенные здесь значения:
Name:             msa-core-f7d95d75b-xhpfx
Namespace:        msa-core
Priority:         0
Service Account:  msa-core-sa #Service Account - совпадает
---
Mounts:
      /etc/nginx/conf.d/default.conf from msa-configmap-storage (rw,path="default.conf")
      /mnt from msa-core-storage (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-b8x4d (ro)
#Прописанные в Deployment пути смонтированы
---
Volumes:
  msa-core-storage:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace) #PVC привязан
    ClaimName:  msa-core-pvc
    ReadOnly:   false
  msa-configmap-storage:
    Type:      ConfigMap (a volume populated by a ConfigMap) #CM привязан
    Name:      msa-core-cm
    Optional:  false
```
Проверим создались ли Secrets
```bash
kubectl get secrets
NAME                             TYPE                             DATA   AGE
msa-core-sec                     kubernetes.io/dockerconfigjson   1      100m
sh.helm.release.v1.msa-core.v1   helm.sh/release.v1               1      100m
## Выведем содержимое секрета:
kubectl get secret msa-core-sec -o jsonpath='{.data.*}' | base64 -d
## Флаг -d значит декодировать или ввести --decode
## Вывод
{"auths":{"quay.io":{"username":"someone","password":"sillyness","email":"someone@host.com","auth":"c29tZW9uZTpzaWxseW5lc3M="}}}
## msa-core-sec - имя вашего секрета
```
Так как у нас был создан Servive, посмотрим на его состояние тоже:
```bash
kubectl get svc
#Вывод:
NAME          TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
msa-core     NodePort    10.107.160.242   <none>        8080:32554/TCP   24m

#msa-core сервис работает, можно попробовать к нему подлюкчиться с помощью браузера.
#Тип указан - NodePort;
#Указываем IP - адрес хоста (ПК или виртуальной машины) с которого производились все действия.
#Узнать свой IP адрес можно с помощью команды ip addr show | grep eth
#Или ip a | grep eth, обычно он начинается с 192.168....
Введём в строке браузере 
http://Ваш IP:32554/
#Вывод:
hello perfect world
```
Также можно подключиться к своей поде и проверить работают ли монтированные тома. /mnt и /etc/nginx/conf.d/default.conf

```bash
kubectl get pods
#Вывод:
NAME                       READY   STATUS      RESTARTS   AGE
msa-core-f7d95d75b-xhpfx   1/1     Running     0          26m
#Подключаемся к ПОДу:
kubectl exec -it pod/msa-core-f7d95d75b-xhpfx -- sh
cd mnt
#По этому же пути заходим с машины хоста и смотрим какие изменения можно вносить в каталог /mnt
#msa-core-f7d95d75b-xhpfx - название вашего ПОДа
```

# Развёртывание Helm Chart в кластере Kubernetes с использованием Gitlab CI/CD

--- 

## Необходимые условия:
- Установленный кластер Kubernetes https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/  ;
- Установленный менеджер пакетов Helm  https://helm.sh/docs/intro/install/ ;
- Работающий Gitlab agent https://gitlab.com/gitlab-org/charts/gitlab-agent или https://docs.gitlab.com/ee/user/clusters/agent/install/index.html;
- Работающий Gitlab runner https://gitlab.com/gitlab-org/charts/gitlab-runner  или https://docs.gitlab.com/runner/install/kubernetes.html. 
У ранера отключаем Enable shared runners for this project.

В gitlab runner в файле values.yaml, необходимо ввести следующие значения:
```bash
sudo vi values.yaml

gitlabUrl: https://neogit.neoflex.ru/ #Адрес вашего Git - репозитория
runnerRegistrationToken: "GR1348941VEzQzJdxePxvkTcMwdyN" #Токен вашего раннера
Не перепутайте с #runnerToken: ""
rbac:
  create: true #Задаём значение true для создания ролей.
```
## 1. Файл .gitlab-ci.yaml
Перед созданием файла .gitlab-ci.yaml склонируем к себе готовый проект: https://neogit.neoflex.ru/DevOps/msa-platform/bootcamp-idrisov.git
Создадим папку `deploy-test`:
```bash
mkdir deploy-test
cd deploy-test
git clone https://neogit.neoflex.ru/DevOps/msa-platform/bootcamp-idrisov.git #Клонируем репозиторий
cd bootcamp-idrisov
```
Выведем содержимое папки:
```bash
.git/
.gitlab/
.gitlab-ci.yml #У нас данный файл уже имеется, можно его удалить и попробовать написать по шаблону ниже
gitlab-runner-values.yaml
golang-app/               
kubetask1/
kubetask2/
msa-core/ #Здесь находится наш helm chart
nodejs-app/
README.md
```
Файл `.gitlab-ci.yml` используется для настройки CI/CD из корня репозитория.

Gitlab runner выполняет команды описанные в файле .gitlab-ci.yml.
Результат работы и выполнение команд gitlab runner отображаются на вкладке jobs или также можно посмотреть pipelines.

Вкладки jobs и pipelines находятся по пути:
`CI/CD > Jobs` или `CI/CD > Pipelines`


Написанный файл `.gitlab-ci.yml` был создан для следующего проекта: https://neogit.neoflex.ru/DevOps/msa-platform/bootcamp-idrisov
Helm chart находится в папке msa-core.

```bash
sudo vi .gitlab-ci.yml

stages: #Этапы (шаги), зададим им названия
  - deploy #Зададим имя этапу Развёртывание

deploy-job:
  stage: deploy #Указываем название этапа (шага), заданного ранее
  image: alpine/helm:3.10.2 #Требуемый образ
  before_script: #Действия до выполнения основного скрипта
    - ls
  
  script:
    - helm upgrade --install msa-core . --namespace msa-core --create-namespace --atomic #Устанавливаем приложение из нашего репозитория задав ему имя msa-core
    - echo "Hello, $GITLAB_USER_LOGIN!, JOB is DONE" #Вывод сообщения для мониторинга действий
```
Рассмотрим подробно: `helm upgrade --install ${CHART_NAME} msa-core/msa-core --namespace msa-core --create-namespace --atomic` 
```bash
helm upgrade --install - если чарт уже развёрнут, то он будет обновлён (upgrade), если чарт не был развёрнут, то он будет установлен (install);
${CHART_NAME} - имя прописанное в Variables, msa-core;
msa-core/msa-core -  название репозитория из которого необходимо развернуть Helm chart;
--namespace msa-core --create-namespace - создать и поместить в пространство имён msa-core;
--atomic - флаг, позволяющий автоматически мониторить установку чарта, задаётся флаг wait. Helm ожидает 5 минут, если приложение развёрнуто, то всё хорошо, если нет, то приложение откатывается на предыдущий релиз или версию.
```

## 2. Отправка проекта в репозиторий. Развёртывание проекта с использованием CI/CD Gitlab

### 2.1 Отправка проекта в репозиторий Gitlab
После того, как были выполнены все изменения отправляем их в наш репозиторий.
Перед отправкой необходимо выполнить следующие действия:
```bash
#Добавление файлов в индекс
git add . или git add .gitlab-ci.yml #.gitlab-ci.yml наименование изменённого или созданного файла.
#Команды выполняются в каталоге с проектом, где находится папка .git
git status #Проверяем добавились ли изменения в индекс
git commit -m "edit ci.yml" #Добавление изменений в репозиторий
#Флаг -m - указывает на добавление комментария
#"edit ci.yml" - комментарий
git remote add origin https://neogit.neoflex.ru/DevOps/msa-platform/bootcamp-idrisov.git #Добавляем удалённый репозиторий
git branch -M main #Переименовываем ветку в main
git push -uf origin main #Отправляем все изменения в наш центральный репозиторий в https://neogit.neoflex.ru/

#Возможно будут запрошены логин и пароль от репозитория
```
### 2.2 Мониторинг работы CI/CD Gitlab

Проект отправлен. Теперь необходимо проверить работает ли наш pipeline.
**Pipeline** - это последовательное выполнение прописанных нами stages (шагов, стадий или этапов) в файле .gitlab-ci.yml
**Stage** - включает в себя задачи (Job).
**Job** - Некая отдельно выполняемая задача. Например: скрипт, команда, утилита и др.

Посмотреть как отработал файл .gitlab-ci.yml можно несколькими способами:

**Первый способ**. Перейти в свой репозиторий. В нём будет отображена информация о состоянии выполнения pipeline.
![](./images/repo-pipe.jpg)
На представленном изображении мы видим зелёную галочку. Она кликабельна, нажимаем на неё и будет отображена работа нашего pipeline. 
![](./images/repo-pipe-dep.jpg)
Переходим в deploy-job.
Здесь мы можем подробнее рассмотреть отработанный pipeline.

**Ошибка** 
WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /builds/youred/msa-core.tmp/KUBECONFIG, говорит нам о том, что по пути файла /builds/youred/msa-core.tmp/KUBECONFIG права на чтение и запись выданы всем. Данная ошибка не критична.
**Решение**
Выдать права на чтение и записаь только для пользователя:
chmod 600 /builds/DevOps/msa-platform/bootcamp-idrisov.tmp/KUBECONFIG

**Возможно будет выведена ошибка** 
Error: INSTALLATION FAILED: rendered manifests contain a resource that already exists. Unable to continue with install: could not get information about the resource ServiceAccount "msa-core-sa" in namespace "gitlab-runner": serviceaccounts "msa-core-sa" is forbidden: User "system:serviceaccount:gitlab-runner:default" cannot get resource "serviceaccounts" in API group "" in the namespace "gitlab-runner"

Необходимо запомнить. Что после установки GitLab runner, используется Kubernetes executor. Это означает, что этот исполнитель GitLab создаст модуль в пространстве имен кластера kubernetes для каждого задания в каждом конвейере. Поэтому для этого ему нужен доступ к Kubernetes API.

Когда gitlab runner установлен в этом пространстве имен, будет использоваться служебная учетная запись system:serviceaccount:gitlab-runner:default (это служебная учетная запись в пространстве имен btcamp-gitlab-runner). У этой служебной учетной записи нет никаких разрешений, поэтому gitlab runner не сможет создать какой-либо модуль.

**Решение**
https://kubernetes.io/docs/reference/access-authn-authz/rbac/

```bash
kubectl create clusterrolebinding gitlab-cluster-admin --clusterrole=cluster-admin --group=system:serviceaccounts --namespace=btcamp-gitlab-runner
```

**Второй способ**. Переходим в `CI/CD > Pipelines`
![](./images/repo-pipe-ci-cd.jpg)
Затем кликаем по вкладке passed и переходим в наш pipeline. Дальнейшие действия происходят аналогично первому способу.

**Третий способ** Переходим в `CI/CD > Jobs`
Затем кликаем по вкладке passed и перед нами сразу будет выведена информация о выполненных командах.

После успешного выполнения pipeline переходим в наш терминал и проверяем работу приложения.

Посмотрим на наши ПОДы:
```bash
kubectl get pods -A #Команда выводит все имеющиеся ПОДы
#Или можем посмотреть только наш проект
kubectl get pods -n msa-core
#Вывод:
NAME                        READY   STATUS    RESTARTS   AGE
msa-core-748f457b6c-zghxr   1/1     Running   0          2d17h
```
Проверяем связались ли наши Persistent Volumes и Claim:
```bash
kubectl get pv
NAME          CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                      STORAGECLASS    REASON   AGE
msa-core-pv   3Gi        RWO            Retain           Bound    msa-core/msa-core-pvc          msa-core            2d17h

#Name  - msa-core-pvc
#Status bound - связаны
#Claim - msa-core/msa-core-pvc


kubectl get pvc -n msa-core
NAME           STATUS   VOLUME        CAPACITY   ACCESS MODES   STORAGECLASS   AGE
msa-core-pvc   Bound    msa-core-pv   3Gi        RWO              msa-core    2d17h

#Name - msa-core-pvc
#Status bound - связаны
#Volume - msa-core-pv

#Как видно из полученных данных pv и pvc связаны между собой
```
Проверим работу сервиса:
```bash
kubectl get svc -n  msa-core
NAME       TYPE       CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
msa-core   NodePort   10.104.182.44   <none>        8080:32554/TCP   2d17h

curl 10.104.182.44:8080
#Вывод:
hello perfect World

curl Ваш_IP:32554
#Вывод:
hello perfect World
```
Проверяем работу в браузере:
`Ваш_IP:32554`

![](./images/ip-test.jpg)
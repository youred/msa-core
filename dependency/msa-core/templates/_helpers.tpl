{{/*
Expand the name of the chart.
*/}}
{{- define "msa-core.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "msa-core.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "msa-core.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "msa-core.labels" -}}
helm.sh/chart: {{ include "msa-core.chart" . }}
{{ include "msa-core.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "msa-core.selectorLabels" -}}
app.kubernetes.io/name: {{ include "msa-core.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "msa-core.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "msa-core.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

#{{/*
#Create the name of the persistent volume to use
#*/}}
#{{- define "msa-core.persistentVolume" -}}
#{{- if .Values.persistentVolume.create }}
#{{- default (include "msa-core.fullname" .) .Values.persistentVolume.name }}
#{{- else }}
#{{- default "default" .Values.persistentVolume.name }}
#{{- end }}
#{{- end }}

#{{/*
#Create the name of the persistent volume claim to use
#*/}}
#{{- define "msa-core.persistentVolumeClaim" -}}
#{{- if .Values.persistentVolumeClaim.create }}
#{{- default (include "msa-core.fullname" .) .Values.persistentVolumeClaim.name }}
#{{- else }}
#{{- default "default" .Values.persistentVolumeClaim.name }}
#{{- end }}
#{{- end }}

#{{/*
#Create the name of the config map to use
#*/}}
#{{- define "msa-core.configMap" -}}
#{{- if .Values.configMap.create }}
#{{- default (include "msa-core.fullname" .) .Values.configMap.name }}
#{{- else }}
#{{- default "default" .Values.configMap.name }}
#{{- end }}
#{{- end }}

{{/*
Create the name of the image Pull Secret to use
*/}}
{{- define "imagePullSecret" }}
{{- with .Values.imageCredentials }}
{{- printf "{\"auths\":{\"%s\":{\"username\":\"%s\",\"password\":\"%s\",\"email\":\"%s\",\"auth\":\"%s\"}}}" .registry .username .password .email (printf "%s:%s" .username .password | b64enc) | b64enc }}
{{- end }}
{{- end }}
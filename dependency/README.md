# Helm dependencies

Официальная документация: https://helm.sh/docs/helm/helm_dependency/

В Helm имеется возможность представления зависимости одного чарта от другого. 
Работать будем с Spring Boot и KeyDB.

**Spring Boot** – это дополнение к Spring, которое облегчает и ускоряет работу с ним.
**Spring** - это фреймворк (набор инструментов) для Java-платформ или для языка программирования Java.  

**KeyDB** - это база данных. Ответвление от Redis. Работает быстрее Redis.
Имеет возможность вертикального масштабирования и использования нескольких носителей данных.

## Сборка Helm dependencies

Helm поддерживает технологию модульной разбработки чартов и сабчартов (поддиаграммы). Работает данная технология следующим образом: у нас имеется основной чарт к которому мы можем подключать типовые чарты.
Для подключения сабчаротов необходимо указать зависимости (dependencies) в файле Chart.yaml:
У нас уже имеется проект msa-core, будем работать с ним:
Перейдём в папку msa-core и откроем файл Chart.yaml
```bash
sudo vi Chart.yaml

apiVersion: v2
name: msa-core
description: A Helm chart for Kubernetes

# A chart can be either an 'application' or a 'library' chart.
#
# Application charts are a collection of templates that can be packaged into versioned archives
# to be deployed.
#
# Library charts provide useful utilities or functions for the chart developer. They're included as
# a dependency of application charts to inject those utilities and functions into the rendering
# pipeline. Library charts do not define any templates and therefore cannot be deployed.
type: application

# This is the chart version. This version number should be incremented each time you make changes
# to the chart and its templates, including the app version.
# Versions are expected to follow Semantic Versioning (https://semver.org/)
version: 0.1.0

# This is the version number of the application being deployed. This version number should be
# incremented each time you make changes to the application. Versions are not expected to
# follow Semantic Versioning. They should reflect the version the application is using.
# It is recommended to use it with quotes.
appVersion: "1.16.0"

dependencies:
- name: spring-boot
  version: "0.1.0"
  repository: "file://../spring-boot"
  condition: spring-boot.enabled
- name: keydb
  version: "0.43.1" 
  repository: "file://../keydb"
  condition: keydb.enabled
```
```bash
Описание значений в chart.yaml
## dependencies: ## Здесь задаются зависимости
## name: spring-boot Задаём имя 
## version: "0.1.0" Версию чарта, она представлена в chart.yaml самого проекта или на сайте репозитория Chart version
## "file://../spring-boot" локальное использование чарта, также моэно указать репозиторий для скачивания https://cnieg.github.io/helm-charts
## condition: prometheus.enabled Состояние, задаётся в values yaml
## repository: "file://../keydb" Репозиторий для скачивания, здесь указано скасчивание локальное, чарт находится на нашем компьютере или можно указать https://enapter.github.io/charts/
## condition: keydb.enabled  Состояние, задаётся в values yaml
```
Репозиторий для скачивания spring-boot: https://artifacthub.io/packages/helm/cnieg/spring-boot

Репозиторий для скачивания KeyDB: https://artifacthub.io/packages/helm/enapter/keydb или https://github.com/Enapter/charts

Наш каталог будет содержать следующее: 
```bash
ll
## Вывод
msa-core
keydb
spring-boot
```
В каталоге msa-core задаём значения в файле values.yaml для spring-boot и keyDB , будет показана часть файла:
```bash
sudo vi values.yaml

replicaCount: 1

## Задаём глобальные значения для наши сабчартов.
## Например у всех сабчартов (дочерних чартов) будет задано имя Storage Class Name: msa-core
global:
  storageClass: "msa-core"

## Примечание! 
## Чарты бывают родительские от которых всё наследуется и дочерние, 
## которые используют параметры заданные в родительских чартах. 
## Также родительские чарты могут использовать параметры дочерних чартов
## Для этого необходимо задать import-values в файле Chart.yaml
## Документация тут: https://helm.sh/docs/topics/charts/

## Параметры задаваемые в values.yaml в родительском чарте для дочерних сабчартов 
## должны совпадать со значениями в values.yaml самих дочерних чартов 
## Например в values.yaml значению :      Соответсвует значению в чарте keydb values.yaml:
      # keydb:                                
      #   enabled: true                       
      #   nodes: 1                            nodes: 1                     
  
      #   persistentVolume:                   persistentVolume:
      #     enabled: true                       enabled: true
      #     accessModes:                        accessModes:

## Дочерний сабчарт spring-boot
## Указываем, что наш сабчарт keydb включен или выключен, параметр enable
spring-boot: ## Имя должно соответствовать с dependencies в Chart.yaml
  enabled: true
  replicaCount: 1 ## Указываем количество копий
  service: 
    type: NodePort ## Тип сервиса
    port: 80 ## Номер порта

# Дочерний сабчарт keydb
## Указываем, что наш сабчарт keydb включен или выключен
keydb:
  enabled: true
  nodes: 1 ## количество копий, можно задать replicaCount
  
  persistentVolume: ## Указываем, что создание томов включено
    enabled: true 
    accessModes: 
      - ReadWriteOnce ## Задаём параметры доступа
    selector: {}
    # matchLabels:
    #   release: "stable"
    # matchExpressions:
    #   - {key: environment, operator: In, values: [dev]}
    size: 1Gi ## Размер хранилища
    storageClass: "msa-core" ## И задаём имя storage class, должно соответствовать с именем заданным в родительском чарте.
    
  loadBalancer: ## Тип сервиса, включить
    enabled: false                    
    
image:
  repository: nginxinc/nginx-unprivileged
  pullPolicy: IfNotPresent
  tag: "1.23.3"

```
После всех проделанных действий находясь в каталоге msa-core выполним следующие команды:
```bash
helm dependency list ## Выведем список зависимостей

## Вывод:
NAME            VERSION REPOSITORY                                              STATUS
springboot      0.1.0   file://../springboot                                    missing
keydb           0.43.1  file://../keydb                                         missing

## Видим, что имя, версии и месторасположение соответсвуют,
## но статус стоит отсутствует
```
Вводим команду для обновления зависимостей:
```bash
helm dependency update или helm dep update

## Если все пути и параметры указаны верно, то будет выведено сообщение:
Hang tight while we grab the latest from your chart repositories...
...Successfully got an update from the "enapter" chart repository
...Successfully got an update from the "pozetron" chart repository
...Successfully got an update from the "cilium" chart repository
Update Complete. ⎈Happy Helming!⎈
Saving 2 charts
Deleting outdated charts
```
Повторно выведем список зависимостй:
```bash
helm dep list
NAME            VERSION REPOSITORY              STATUS
spring-boot     0.1.0   file://../spring-boot   ok
keydb           0.43.1  file://../keydb         ok

## Статус ок, значит всё ок)
```
После того как выполнили представленные выше команды у нас будет сгенерирована папка charts с чартами запакованными в архив и файл Chart.lock:
```bash
## Посмотрим вывод:
ll

./
../
Chart.lock
charts/
Chart.yaml
CI-Gitlab.md
files/
.helmignore
images/
README.md
templates/
values.yaml

## Перейдём в папку charts
cd charts
## Выведеим список файлов
ll
## Видим, что сгенерировано два архива
keydb-0.43.1.tgz
spring-boot-0.1.0.tgz
```
Посмотрим файл Chart.lock, файл находится в папке msa-core, вернёмся из каталога charts обратно в каталог msa-core:
```bash
cd ..
ll

./
../
Chart.lock
charts/
Chart.yaml
CI-Gitlab.md
files/
.helmignore
images/
README.md
templates/
values.yaml

sudo vi Chart.lock

## Вывод файла:
dependencies:
- name: spring-boot
  repository: file://../spring-boot
  version: 0.1.0
- name: keydb
  repository: file://../keydb
  version: 0.43.1
digest: sha256:d0c4b84dc83436bd4f781dac406106b9a98f6069538df5af647f55eab5d725ad
generated: "2023-01-16T08:25:42.571949533Z"

## Видим, что добавлены значения наших сабчартов (подчартов)
## Выходим из файла.
```
Запускаем установку наших чартов:
```bash
helm install -f values.yaml msa-core .
## -f - указываем файл values.yaml
## msa-core - название Helm чарта

## Вывод:
NAME: msa-core
LAST DEPLOYED: Mon Jan 16 08:25:53 2023
NAMESPACE: default
STATUS: deployed
REVISION: 1
NOTES:
1. Get the application URL by running these commands:
  export NODE_PORT=$(kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services msa-core)
  export NODE_IP=$(kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
  echo http://$NODE_IP:$NODE_PORT
```
Посмотрим список Чартов:
```bash
helm list

##Вывод:
NAME            NAMESPACE       REVISION        UPDATED                                 STATUS          CHART           APP VERSION
msa-core        default         1               2023-01-16 08:25:53.618694715 +0000 UTC deployed        msa-core-0.1.0  1.16.0
```
Проверим список ПОД:
```bash
kubectl get pods

##Вывод:
kubectl get pods
NAME                                    READY   STATUS      RESTARTS   AGE
msa-core-59b469c7f5-8vlmx               1/1     Running     0          47m
msa-core-keydb-0                        1/1     Running     0          47m
msa-core-spring-boot-8677c4f45d-4lt65   1/1     Running     0          47m

## Поды работают
```
**Примечание** не забываем создать дополнительный persistent volume для каждого persistent volume claim.



Также все values мы можем переопределять при вызове Helm при помощи ключа --set:
Например:
```bash
helm install msa-core . --set global.storageClass=msa-core \   ## Задаём название storage class для всех сабчартов
--set keydb.nodes=1 \              ## Указаываем количество копий (реплик) для сабчарта keydb
--set keydb.loadBalancer.enabled="true"    ## Указываем, что необзодимо включить тип сервиса load Balancer
```